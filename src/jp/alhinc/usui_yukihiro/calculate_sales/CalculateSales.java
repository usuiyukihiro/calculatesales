package jp.alhinc.usui_yukihiro.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;


public class CalculateSales {
	
	public static void main(String[] args){
		if(args.length != 1){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}		
		
		String dirPath = args[0];
		HashMap<String, String> nameMap = new HashMap<String, String>();
		HashMap<String, String> salesMap = new HashMap<String, String>();
		String nameFormat = "支店";
		String fileFormat = "^[0-9]{3},[^,]+$";
		
		//支店定義ファイルの読み込み//		
		if(!(readFile(dirPath, "branch.lst", nameMap, salesMap, nameFormat, fileFormat))){
			return;
		}
		
		//売り上げファイル抽出//
		String[] fileName = null;
		FileReader r_fr = null;
		BufferedReader r_br = null ;
		
		try{
			File calculateFile = new File(dirPath);
			File[] fileAddress = calculateFile.listFiles();
			ArrayList<String> rcdList = new ArrayList<String>();
			String s;

			//Dirから読込//
			for(int i = 0;i < fileAddress.length;i++){
				File item = fileAddress[i];
				if(item.isFile()){
					//正規表現 (8桁の数字.rcdになっているか)//
					if((fileAddress[i].getName()).matches("[0-9]{8}.rcd")){
						rcdList.add(fileAddress[i].getName());
					}
				}
			}
			//連番チェック//
			for(int i = 0;i < rcdList.size()-1;i++){
				
				fileName = (rcdList.get(i)).split(".rcd");
				int fileNumber = Integer.parseInt(fileName[0]);
				
				fileName = (rcdList.get(i+1)).split(".rcd");
				int fileCounter = Integer.parseInt(fileName[0]);
				
				if(((fileCounter - fileNumber) != 1 )){
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}
			}

			//ファイル開く→集計//
			for(int i = 0;i < rcdList.size();i++){
				File calculateReader =new File(args[0],rcdList.get(i));
				r_fr = new FileReader(calculateReader);
				r_br = new BufferedReader(r_fr);
				ArrayList<String> calculateList = new ArrayList<String>();

				//支店コードチェック//
				while((s = r_br.readLine()) != null){
					calculateList.add(s);;
				}
				if(!(nameMap.containsKey(calculateList.get(0)))){
					System.out.println(rcdList.get(i)+"の支店コードが不正です");
					return;
				}
				if((calculateList.size() != 2)){
					System.out.println(rcdList.get(i)+"のフォーマットが不正です");
					return;
				}
				if(!calculateList.get(1).matches("^[0-9]+$")){
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				//数字を数値に変換//
				String mapChar = salesMap.get(calculateList.get(0));
				String listChar = calculateList.get(1);
				long mapValue = Long.parseLong(mapChar);
				long listValue = Long.parseLong(listChar);

				//集計//
				long total = mapValue + listValue;

				//桁数チェック//
				int totallen = String.valueOf(total).length();
				if(totallen > 10){
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				//数値を数字に変換//
				String calulate = String.valueOf(total);
				salesMap.put(calculateList.get(0),calulate);

			}

		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return;

		}finally{
			try{
				if(r_br != null){
				r_br.close();
				}
			}catch(IOException e){
				System.out.println("予期せぬエラーが発生しました");
				return;
			}
		}

		//出力//
		if(!(writeFile(dirPath, "branch.out", nameMap, salesMap))){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
	
	}

	//支店定義ファイル読込メソッド//
	public static boolean readFile(String dirPath, String lstFile, HashMap<String, String>nameMap, HashMap<String, String>salesMap, String nameFormat, String fileFormat){
		
		FileReader fr = null;
		BufferedReader br = null;
		
		try{
			File file = new File(dirPath,lstFile);
			if(!(file.exists())){
				System.out.println(nameFormat+"定義ファイルが存在しません");
				return false;
			}
			fr = new FileReader(file);
			br = new BufferedReader(fr);
			String branchline;

			while((branchline = br.readLine()) != null){
				if(branchline.matches(fileFormat)){
					String[] array = branchline.split(",",-1);
					nameMap.put(array[0], array[1]);
					salesMap.put(array[0], "0");

				}else{
					System.out.println(nameFormat+"定義ファイルのフォーマットが不正です");
					return false;
				}
			}
			
		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return false;

		}finally{
			try{
				if(br != null){
				br.close();
				}
			}catch(IOException e){
				System.out.println("予期せぬエラーが発生しました");
				return false;
			}
		}
		return true;
	}
	
	//出力メソッド//
	public static boolean writeFile(String dirPath, String outFile, HashMap<String, String>nameMap, HashMap<String, String>salesMap){
		
		FileWriter fw = null;
		BufferedWriter bw = null;
		
		try{
			File outfile = new File(dirPath, outFile);
			fw = new FileWriter(outfile);
			bw = new BufferedWriter(fw);
		
			//プラットフォームに適した改行コード//
			String sep = System.getProperty("line.separator");

			for(String key : nameMap.keySet()){
				bw.write(key + "," + nameMap.get(key) + "," + salesMap.get(key) + sep);
			}			
		}catch(IOException e){
			return false;

		}finally{
			try{
				if(bw != null){
				bw.close();
				}
			}catch(IOException e){
				return false;
			}
		}
		return true;
	}
}	
